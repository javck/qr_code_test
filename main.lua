-----------------------------------------------------------------------------------------
--
-- main.lua
-- 本範例示範如何將QR Code功能導入Corona Project，更多細節請參考官網：http://spiralcodestudio.com/plugin-qrscanner/
-- Step 1.到Corona Store去購買該Plugin(https://store.coronalabs.com/plugin/qr-scanner)
-- Step 2.在build.settings註冊plugins
-- Step 3.使用require載入plugin
-- Step 4.開始使用..
-- 
-- Author: 林品爵 
-- Time: 2016/2/23
--
-----------------------------------------------------------------------------------------
--載入QR Code Plugin
local qrscanner = require('plugin.qrscanner')

SCREEN = {
	WIDTH = display.viewableContentWidth,
	HEIGHT = display.viewableContentHeight
}
SCREEN.CENTER = {
	X = display.contentCenterX,
	Y = display.contentCenterY
}

local btn

local onClickQR_Code
local listener
local options = {}

--自訂QR Code Scanner的訊息
options.strings = {  
    title = 'QR Scanner',
    no_permission_err = '本App尚未得到鏡頭的授權',
    no_camera_err = '該裝置偵測不到鏡頭',
    unknown_error_err = '發生不明原因錯誤...',
    error_dialog_title = '沒有掃描到Code',
    error_dialog_button = '確認'
}


btn = display.newImageRect(  "images/PlayButton.png", 300, 200 )
btn.anchorx = btn.width / 2
btn.anchory = btn.height / 2
btn.x = SCREEN.CENTER.X
btn.y = SCREEN.CENTER.Y

--QR Code程式偵聽器
listener = function(message)  
    -- message variable contains the value of a scanned QR Code or a barcode.
    -- 所傳入message即為QR Code的內容
    native.showAlert('QR Code Scanner', message, {'OK'})
end

onClickQR_Code = function ( )
	-- 開啟QR Code Scanner
	qrscanner.show(listener , options)  
end

btn:addEventListener( "tap", onClickQR_Code )
